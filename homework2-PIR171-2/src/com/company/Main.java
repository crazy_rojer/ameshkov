package com.company;
import java.util.Scanner;
/**
 * This class is the solution of homework №2
 */
public class Main {

    public static void main(String[] args) {
        int algorithmId = 3;
        int loopType;
        int param;
        Scanner in = new Scanner(System.in);
        System.out.print("Input a task number: ");
        int tasknumber = in.nextInt();
        if (tasknumber == 1) {
            calculateG();
        }
        else if(tasknumber == 2) {
            System.out.print("Input a algorithm Id: ");
            algorithmId = in.nextInt();
            System.out.print("Input a loop type: ");
            loopType = in.nextInt();
            System.out.print("Input a parameter: ");
            param = in.nextInt();
            if (param < 0){
                System.out.print("Incorrect parameter: ");
            }
            else {
                switch (algorithmId) {
                    case 1:
                        calculateFibonacci(loopType, param);
                        break;
                    case 2:
                        calculateFactorial(loopType, param);
                        break;
                }
            }
        }
        else System.out.print("Incorrect task number");
    }

    public static void calculateG()
    /**
     * This method is the solution of task №1
     * the value of G is calculated by the given arguments
     */
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number a: ");
        int a = in.nextInt();
        System.out.print("Input a number p: ");
        int p = in.nextInt();
        System.out.print("Input a number m1: ");
        double m1 = in.nextDouble();
        System.out.print("Input a number m2: ");
        double m2 = in.nextDouble();
        double g = 4 * java.lang.Math.PI * java.lang.Math.pow(a,3) / java.lang.Math.pow(p,2) / (m1 + m2);
        System.out.print(g);
    }

    public static void calculateFactorial(int cicleType, int param)
    /**
     * This method is the solution of task №2
     * factorial is calculated by the specified cycle type and parameter
     */
    {
        int counter = 1;
        int result = 1;
        switch (cicleType) {
            case 1:
                counter = param;
                while (counter > 1) {
                    result *= counter--;
                }
                break;
            case 2:
                do {
                    result *= counter++;
                } while (counter <= param);
                break;
            case 3:
                for (counter = 1; counter <= param; counter++) {
                    result *= counter;
                }
                break;
        }
        System.out.println(result);
    }

    public static void calculateFibonacci(int cicleType, int param)
    /**
     * This method is the solution of task №2
     * displays a series of Fibonacci numbers for a given cycle type and parameter
     */
    {
        int num1 = 0;
        int num2 = 1;
        int counter = 0;
        System.out.print(num1 + " " + num2 + " ");
        int result = 0;
        switch (cicleType){
            case 1:while (counter < param - 2) {
                System.out.print(num1 + num2 + " ");
                num2 += num1;
                num1 = num2 - num1;
                counter++;
                }
                break;
            case 2:do {
                System.out.print(num1 + num2 + " ");
                num2 += num1;
                num1 = num2 - num1;
                counter++;
                } while (counter < param - 2);
                break;
            case 3: for(int i = 0; i < param - 2; i++) {
                System.out.print(num1 + num2 + " ");
                num2 += num1;
                num1 = num2 - num1;
            }
                break;
        }
        System.out.println();
    }
}
